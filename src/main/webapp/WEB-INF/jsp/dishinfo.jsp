<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Dish info</title>
</head>
<body>
	<div align="center">
		<h2>Dish</h2>
		<f:form action="${dish.id == null ? '/dishes?action=create' : '/dishes/'.concat(dish.id).concat('?action=update')}"
			 method="post" modelAttribute="dish" enctype="multipart/form-data">
			<table border="0" cellpadding="5">
				<tr><td>Name: </td><td><f:input path="name"/></td></tr>
				<tr><td>Price: </td><td><f:input path="price"/></td></tr>
				<tr><td>KCal: </td><td><f:input path="kcal"/></td></tr>
				<tr><td>Description: </td><td><f:input path="description"/></td></tr>
				<tr><td colspan="2"><input type="submit" value="Save" /></td>
			</table>
		</f:form>
	</div>
</body>
</html>