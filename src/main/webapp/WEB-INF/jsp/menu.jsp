<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Menu</title>
</head>
<body>
	<div align="center">
		<h2>Menu</h2>
		<table border="1" cellpadding="5">
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Price</th>
				<th>KCal</th>
				<th>Description</th>
				<th>ManagerId</th>
				<th>Image</th>
				<th>Actions</th>
			</tr>
			<c:forEach items="${dishes}" var="dish">
				<tr>
					<td>${dish.id}</td>
					<td>${dish.name}</td>
					<td>${dish.price}</td>
					<td>${dish.kcal}</td>
					<td>${dish.description}</td>
					<td>--<!-- TODO: managerID --></td>
					<td><!-- TODO: image --></td>
					<td><a href="/dishes/${dish.id}?action=edit&mode=htmlForm">Edit</a>
						<a href="/dishes/${dish.id}?action=delete&mode=htmlForm">Delete</a>
					</td>
				</tr>
			</c:forEach>
		</table>
		<a href="/dishes?action=create&mode=htmlForm">Create dish</a>
	</div>
</body>
</html>