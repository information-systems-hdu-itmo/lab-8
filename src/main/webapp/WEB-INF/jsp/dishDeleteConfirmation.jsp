<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Dish info</title>
</head>
<body>
	<div align="center">
		<h2>Dish</h2>
			<table border="0" cellpadding="5">
				<tr><td>Name: </td><td>${dish.name}"/</td></tr>
				<tr><td>Price: </td><td>${dish.price}</td></tr>
				<tr><td>KCal: </td><td>${dish.kcal}</td></tr>
				<tr><td>Description: </td><td>${dish.description}</td></tr>
				<tr><td>Image: </td><td> -- <!-- TODO --></td></tr>
				<tr>
					<td colspan="2">
						<form action="/dishes/${dish.id}?action=delete" method="post">
							<input type="submit" value="Delete" />
						</form>
					</td>
			</table>
	</div>
</body>
</html>