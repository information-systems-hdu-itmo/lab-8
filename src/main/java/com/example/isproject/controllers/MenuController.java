package com.example.isproject.controllers;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;


import com.example.isproject.db.entities.Dish;
import com.example.isproject.internals.DishesService;


@Controller
@RequestMapping("/dishes")
public class MenuController {

	private final DishesService dishesService;
		
	public MenuController(DishesService dishesService) {
		this.dishesService = dishesService;
	}
	
	//
	// READ
	//
	
	@GetMapping("")
	@ResponseBody
	public List<Dish> getAllDishes() {
        return dishesService.findAll();
		
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Dish> findDishById(@PathVariable long id) {
		return dishesService.findById(id).map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	}

	@GetMapping(path = "", params = {"view=html"})
	@ResponseBody
	public ModelAndView getAllDishesHtmlView() {
		List<Dish> dishes = dishesService.findAll();
		return new ModelAndView("menu", "dishes", dishes);
	}


	//
	// CREATE
	//
		
	@PostMapping(path = "", params = {"action=create"}, consumes = "multipart/form-data")
	public ResponseEntity<Dish> createNewDish(@RequestParam String name, @RequestParam String description, @RequestParam Double kcal, @RequestParam Double price) {
		Dish newDish = dishesService.create(name, description, kcal, price);
		return ResponseEntity.created(URI.create("/dishes/" + newDish.getId())).body(newDish);		
	}
	
	@GetMapping(path = "", params = { "action=create", "mode=htmlForm" })
	@ResponseBody
	public ModelAndView getCreateNewDishHtmlForm() {
		return new ModelAndView("dishinfo", "dish", new Dish(null, null, null, null, null, null, null));
	}
	
	//
	// UPDATE
	//
		
	@PostMapping(value = "/{id}",params = {"action=update"},consumes = "multipart/form-data")
	public ResponseEntity<Dish> updateDish(@PathVariable long id, @RequestParam String name, @RequestParam String description, @RequestParam Double kcal, @RequestParam Double price) {
		Optional<Dish> updatedDish = dishesService.updateIfExists(id, name, description, kcal, price);
		return updatedDish.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	}
	
	@GetMapping(path = "/{id}", params = { "action=edit", "mode=htmlForm" })
	@ResponseBody
	public ModelAndView getEditDishHtmlForm(@PathVariable long id) {
		Optional<Dish> dish = dishesService.findById(id);
		if (dish.isPresent()) {
			return new ModelAndView("dishinfo", "dish", dish.get());
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No such dish with id" + id);
		}
	}

	//
	// DELETE
	//
		
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Object> deleteDish(@PathVariable long id) {
		return dishesService.deleteIfExists(id) ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
	}
	
	@PostMapping(value = "/{id}",params = {"action=delete"})
	public ResponseEntity<Object> deleteDishWithPost(@PathVariable long id) {
		return this.deleteDish(id);
	}
	
	@GetMapping(path = "/{id}", params = { "action=delete", "mode=htmlForm" })
	@ResponseBody
	public ModelAndView getDeleteDishHtmlConfirmationForm(@PathVariable long id) {
		Optional<Dish> dish = dishesService.findById(id);
		if (dish.isPresent()) {
			return new ModelAndView("dishDeleteConfirmation", "dish", dish.get());
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No such dish with id" + id);
		}
	}
	
}
