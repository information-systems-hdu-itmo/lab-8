package com.example.isproject.internals;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.isproject.db.entities.Dish;
import com.example.isproject.db.repositories.DishesRepository;

@Service
public class DishesService {
	
	private final DishesRepository dishesRepository;
	
	public DishesService(DishesRepository dishesRepository) {
		this.dishesRepository = dishesRepository;
	}
	
	public List<Dish> findAll() {
		return dishesRepository.findAll() ;
	}

	public Optional<Dish> findById(long id) {
		return dishesRepository.findById(id);
	}

	public Dish create(String name, String description, Double kcal, Double price) {
		Dish newDish = new Dish(null, name, description, kcal, price, null, null);
		return dishesRepository.save(newDish);
	}

	public Optional<Dish> updateIfExists(long id, String name, String description,Double kcal, Double price) {
		Optional<Dish> oldDish = dishesRepository.findById(id);
		if(oldDish.isPresent()) {
			Dish dish = oldDish.get();
			if (name != null) {
				dish.setName(name);
			}
			if (description != null) {
				dish.setDescription(description);
			}
			if (kcal != null) {
				dish.setKcal(kcal);
			}
			if (price != null) {
				dish.setPrice(price);
			}
			return Optional.of(dishesRepository.save(dish));
		} else {
			return Optional.empty();
		}
	}

	public boolean deleteIfExists(long id) {
		Optional<Dish> dish = dishesRepository.findById(id);
		if (dish.isPresent()) {
			dishesRepository.delete(dish.get());
			return true;
		} else {
			return false;
		}
	}


}
